# Phần Mềm Quản Lý Quán Trà Sữa Chinh Chiến
# Tính năng
- Tạo quán vô hạn
- Thêm tài khoản khác vào quán
# Development and TeckStack
- WinForm .NET Framework 4.7.2
- IDE Visual Studio Community 2022
## Giải thích tên các thư mục
- **Classes**: Chứa các lớp như ChuQuan, QuanLy
- **Custom**: Chứa các User Control tùy chỉnh
- **DataSets**: Chứa các DataSet
- **Database**: Chứa các hàm làm việc với Database

# Ideas
- [ ] Tăng cao sự tùy chỉnh để nó có thể trở thành phần mềm quản lý của bất kỳ quán nào.

# Roadmap
Xem trong Discussion..
___
I use this tutorials for help: https://www.youtube.com/watch?v=5AsJJl7Bhvc

# About
Tôi bắt đầu Phân tích và thiết kế phần mềm nay khi học **môn Phân tích và Thiết kế hệ thống** của thầy Phương tại trường Đại Học Bà Rịa Vũng Tàu cùng với 3 người bạn học khác là Trần Trọng Tiến, Lê Quốc Khánh và Hoàng Ngọc Trung
