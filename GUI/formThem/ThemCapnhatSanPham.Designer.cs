﻿namespace ChinChin.GUI.formThem
{
    partial class ThemCapnhatSanPham
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label4 = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblThanhPhan = new System.Windows.Forms.Label();
            this.lblNgaySinh = new System.Windows.Forms.Label();
            this.pnlThongTin = new System.Windows.Forms.Panel();
            this.cbbLoai = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbcDanhGia = new ChinChin.Custom.TextBox_Custom();
            this.tbcGia = new ChinChin.Custom.TextBox_Custom();
            this.tbcTPhan = new ChinChin.Custom.TextBox_Custom();
            this.tbcCT = new ChinChin.Custom.TextBox_Custom();
            this.tbcMaSP = new ChinChin.Custom.TextBox_Custom();
            this.tbcTenSP = new ChinChin.Custom.TextBox_Custom();
            this.btnExit = new FontAwesome.Sharp.IconPictureBox();
            this.btnLuu = new ChinChin.Extra.BlackWhiteButton();
            this.lblThongBao = new System.Windows.Forms.Label();
            this.pnlMoveAndTitle = new System.Windows.Forms.Panel();
            this.lblTitle = new System.Windows.Forms.Label();
            this.pnlBottom = new System.Windows.Forms.Panel();
            this.pnlThongTin.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnExit)).BeginInit();
            this.pnlMoveAndTitle.SuspendLayout();
            this.pnlBottom.SuspendLayout();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Inter", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(14, 561);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(124, 33);
            this.label4.TabIndex = 17;
            this.label4.Text = "Đánh giá";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.BackColor = System.Drawing.Color.Transparent;
            this.lblUserName.Font = new System.Drawing.Font("Inter", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserName.Location = new System.Drawing.Point(14, 17);
            this.lblUserName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(181, 33);
            this.lblUserName.TabIndex = 8;
            this.lblUserName.Text = "Mã sản phẩm";
            this.lblUserName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Inter", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(14, 358);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 33);
            this.label3.TabIndex = 16;
            this.label3.Text = "Giá";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Inter", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(14, 97);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(189, 33);
            this.label2.TabIndex = 10;
            this.label2.Text = "Tên sản phẩm";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblThanhPhan
            // 
            this.lblThanhPhan.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblThanhPhan.AutoSize = true;
            this.lblThanhPhan.BackColor = System.Drawing.Color.Transparent;
            this.lblThanhPhan.Font = new System.Drawing.Font("Inter", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThanhPhan.Location = new System.Drawing.Point(14, 273);
            this.lblThanhPhan.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblThanhPhan.Name = "lblThanhPhan";
            this.lblThanhPhan.Size = new System.Drawing.Size(161, 33);
            this.lblThanhPhan.TabIndex = 14;
            this.lblThanhPhan.Text = "Thành phần";
            this.lblThanhPhan.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblNgaySinh
            // 
            this.lblNgaySinh.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblNgaySinh.AutoSize = true;
            this.lblNgaySinh.BackColor = System.Drawing.Color.Transparent;
            this.lblNgaySinh.Font = new System.Drawing.Font("Inter", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgaySinh.Location = new System.Drawing.Point(14, 188);
            this.lblNgaySinh.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblNgaySinh.Name = "lblNgaySinh";
            this.lblNgaySinh.Size = new System.Drawing.Size(143, 33);
            this.lblNgaySinh.TabIndex = 12;
            this.lblNgaySinh.Text = "Công thức";
            this.lblNgaySinh.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pnlThongTin
            // 
            this.pnlThongTin.Controls.Add(this.cbbLoai);
            this.pnlThongTin.Controls.Add(this.label5);
            this.pnlThongTin.Controls.Add(this.tbcDanhGia);
            this.pnlThongTin.Controls.Add(this.tbcGia);
            this.pnlThongTin.Controls.Add(this.tbcTPhan);
            this.pnlThongTin.Controls.Add(this.tbcCT);
            this.pnlThongTin.Controls.Add(this.label4);
            this.pnlThongTin.Controls.Add(this.lblUserName);
            this.pnlThongTin.Controls.Add(this.label3);
            this.pnlThongTin.Controls.Add(this.tbcMaSP);
            this.pnlThongTin.Controls.Add(this.label2);
            this.pnlThongTin.Controls.Add(this.lblThanhPhan);
            this.pnlThongTin.Controls.Add(this.tbcTenSP);
            this.pnlThongTin.Controls.Add(this.lblNgaySinh);
            this.pnlThongTin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlThongTin.Location = new System.Drawing.Point(0, 55);
            this.pnlThongTin.Name = "pnlThongTin";
            this.pnlThongTin.Size = new System.Drawing.Size(667, 685);
            this.pnlThongTin.TabIndex = 26;
            // 
            // cbbLoai
            // 
            this.cbbLoai.BackColor = System.Drawing.Color.White;
            this.cbbLoai.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbbLoai.Font = new System.Drawing.Font("Cascadia Code", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbLoai.ForeColor = System.Drawing.Color.Black;
            this.cbbLoai.FormattingEnabled = true;
            this.cbbLoai.Items.AddRange(new object[] {
            "Trà sữa",
            "Cà phê",
            "Topping"});
            this.cbbLoai.Location = new System.Drawing.Point(20, 496);
            this.cbbLoai.Name = "cbbLoai";
            this.cbbLoai.Size = new System.Drawing.Size(362, 36);
            this.cbbLoai.TabIndex = 23;
            this.cbbLoai.Text = "Chọn loại";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Inter", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(14, 460);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 33);
            this.label5.TabIndex = 22;
            this.label5.Text = "Loại";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbcDanhGia
            // 
            this.tbcDanhGia.ForeColor = System.Drawing.Color.White;
            this.tbcDanhGia.Location = new System.Drawing.Point(20, 600);
            this.tbcDanhGia.Margin = new System.Windows.Forms.Padding(6);
            this.tbcDanhGia.Name = "tbcDanhGia";
            this.tbcDanhGia.PasswordChar = '\0';
            this.tbcDanhGia.Size = new System.Drawing.Size(362, 40);
            this.tbcDanhGia.TabIndex = 21;
            // 
            // tbcGia
            // 
            this.tbcGia.ForeColor = System.Drawing.Color.White;
            this.tbcGia.Location = new System.Drawing.Point(20, 397);
            this.tbcGia.Margin = new System.Windows.Forms.Padding(6);
            this.tbcGia.Name = "tbcGia";
            this.tbcGia.PasswordChar = '\0';
            this.tbcGia.Size = new System.Drawing.Size(362, 40);
            this.tbcGia.TabIndex = 20;
            // 
            // tbcTPhan
            // 
            this.tbcTPhan.ForeColor = System.Drawing.Color.White;
            this.tbcTPhan.Location = new System.Drawing.Point(20, 312);
            this.tbcTPhan.Margin = new System.Windows.Forms.Padding(6);
            this.tbcTPhan.Name = "tbcTPhan";
            this.tbcTPhan.PasswordChar = '\0';
            this.tbcTPhan.Size = new System.Drawing.Size(362, 40);
            this.tbcTPhan.TabIndex = 19;
            // 
            // tbcCT
            // 
            this.tbcCT.ForeColor = System.Drawing.Color.White;
            this.tbcCT.Location = new System.Drawing.Point(20, 227);
            this.tbcCT.Margin = new System.Windows.Forms.Padding(6);
            this.tbcCT.Name = "tbcCT";
            this.tbcCT.PasswordChar = '\0';
            this.tbcCT.Size = new System.Drawing.Size(362, 40);
            this.tbcCT.TabIndex = 18;
            // 
            // tbcMaSP
            // 
            this.tbcMaSP.BackColor = System.Drawing.Color.White;
            this.tbcMaSP.ForeColor = System.Drawing.Color.White;
            this.tbcMaSP.Location = new System.Drawing.Point(20, 54);
            this.tbcMaSP.Margin = new System.Windows.Forms.Padding(4);
            this.tbcMaSP.Name = "tbcMaSP";
            this.tbcMaSP.PasswordChar = '\0';
            this.tbcMaSP.Size = new System.Drawing.Size(362, 40);
            this.tbcMaSP.TabIndex = 9;
            // 
            // tbcTenSP
            // 
            this.tbcTenSP.ForeColor = System.Drawing.Color.White;
            this.tbcTenSP.Location = new System.Drawing.Point(20, 136);
            this.tbcTenSP.Margin = new System.Windows.Forms.Padding(6);
            this.tbcTenSP.Name = "tbcTenSP";
            this.tbcTenSP.PasswordChar = '\0';
            this.tbcTenSP.Size = new System.Drawing.Size(362, 40);
            this.tbcTenSP.TabIndex = 11;
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExit.BackColor = System.Drawing.Color.Transparent;
            this.btnExit.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnExit.IconChar = FontAwesome.Sharp.IconChar.TimesCircle;
            this.btnExit.IconColor = System.Drawing.SystemColors.ControlText;
            this.btnExit.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnExit.IconSize = 46;
            this.btnExit.Location = new System.Drawing.Point(619, 2);
            this.btnExit.Margin = new System.Windows.Forms.Padding(2);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(46, 48);
            this.btnExit.TabIndex = 29;
            this.btnExit.TabStop = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnLuu
            // 
            this.btnLuu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLuu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(108)))), ((int)(((byte)(247)))));
            this.btnLuu.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(108)))), ((int)(((byte)(247)))));
            this.btnLuu.BorderColor = System.Drawing.Color.Black;
            this.btnLuu.BorderRadius = 0;
            this.btnLuu.BorderSize = 0;
            this.btnLuu.FlatAppearance.BorderSize = 0;
            this.btnLuu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLuu.Font = new System.Drawing.Font("Inter", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.ForeColor = System.Drawing.Color.White;
            this.btnLuu.Location = new System.Drawing.Point(407, 8);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(2);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(258, 50);
            this.btnLuu.TabIndex = 27;
            this.btnLuu.Text = "Lưu";
            this.btnLuu.TextColor = System.Drawing.Color.White;
            this.btnLuu.UseVisualStyleBackColor = false;
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // lblThongBao
            // 
            this.lblThongBao.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblThongBao.BackColor = System.Drawing.Color.Transparent;
            this.lblThongBao.Font = new System.Drawing.Font("Inter", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThongBao.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblThongBao.Location = new System.Drawing.Point(81, 27);
            this.lblThongBao.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblThongBao.Name = "lblThongBao";
            this.lblThongBao.Size = new System.Drawing.Size(322, 19);
            this.lblThongBao.TabIndex = 30;
            this.lblThongBao.Text = "hiện thông báo về mật khẩu và tài khoản";
            this.lblThongBao.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pnlMoveAndTitle
            // 
            this.pnlMoveAndTitle.Controls.Add(this.lblTitle);
            this.pnlMoveAndTitle.Controls.Add(this.btnExit);
            this.pnlMoveAndTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlMoveAndTitle.Location = new System.Drawing.Point(0, 0);
            this.pnlMoveAndTitle.Name = "pnlMoveAndTitle";
            this.pnlMoveAndTitle.Size = new System.Drawing.Size(667, 55);
            this.pnlMoveAndTitle.TabIndex = 31;
            this.pnlMoveAndTitle.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlMoveAndTitle_MouseDown);
            // 
            // lblTitle
            // 
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblTitle.Font = new System.Drawing.Font("Cascadia Code", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(368, 55);
            this.lblTitle.TabIndex = 30;
            this.lblTitle.Text = "Thêm Cập nhật Sản Phẩm";
            this.lblTitle.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lblTitle_MouseDown);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Controls.Add(this.btnLuu);
            this.pnlBottom.Controls.Add(this.lblThongBao);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom.Location = new System.Drawing.Point(0, 740);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(667, 60);
            this.pnlBottom.TabIndex = 32;
            // 
            // ThemCapnhatSanPham
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(667, 800);
            this.ControlBox = false;
            this.Controls.Add(this.pnlThongTin);
            this.Controls.Add(this.pnlBottom);
            this.Controls.Add(this.pnlMoveAndTitle);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "ThemCapnhatSanPham";
            this.pnlThongTin.ResumeLayout(false);
            this.pnlThongTin.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnExit)).EndInit();
            this.pnlMoveAndTitle.ResumeLayout(false);
            this.pnlBottom.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblThanhPhan;
        private System.Windows.Forms.Label lblNgaySinh;
        private System.Windows.Forms.Panel pnlThongTin;
        private Custom.TextBox_Custom tbcDanhGia;
        private Custom.TextBox_Custom tbcGia;
        private Custom.TextBox_Custom tbcTPhan;
        private Custom.TextBox_Custom tbcCT;
        private Custom.TextBox_Custom tbcMaSP;
        private Custom.TextBox_Custom tbcTenSP;
        private FontAwesome.Sharp.IconPictureBox btnExit;
        private Extra.BlackWhiteButton btnLuu;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbbLoai;
        private System.Windows.Forms.Label lblThongBao;
        private System.Windows.Forms.Panel pnlMoveAndTitle;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Panel pnlBottom;
    }
}